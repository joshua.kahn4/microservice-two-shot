import React, { useEffect, useState } from 'react';

function CreateShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: '',
        model: '',
        color: '',
        picture: '',
        bin: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const [error, setError] = useState('');
    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            const url = 'http://localhost:8080/api/shoes/'
            const response = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            if (response.ok) {
                setFormData({
                    manufacturer: '',
                    model: '',
                    color: '',
                    picture: '',
                    bin: '',
                });
            }
            setError('');
        }
        catch (error) {
            console.error('No shoes added:', error.message);
            setError(error.message);
        }
    }

    const handleFormChange = async (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className={'row'}>
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new shoes!</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" value={formData.manufacturer} name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Model" required type="number" value={formData.model} name="model" id="model" className="form-control" />
                            <label htmlFor="room_count">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" required
                                type="text" value={formData.color} name="color" id="color"
                                className="form-control" />
                            <label htmlFor="color" >Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="picture">Picture</label>
                            <textarea onChange={handleFormChange} value={formData.picture} id="picture" rows="3" name="picture" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="bin" value={formData.bin} id="bin" className="form-select">
                                <option >Choose a Bin for your Shoes</option>
                                {
                                    bins.map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default CreateShoeForm;
