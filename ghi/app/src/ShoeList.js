import React, { useEffect, useState } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    async function newShoe() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const { shoes } = await response.json();

            setShoes(shoes);
        }
        else {
            console.error('Error');
        }
    }

    const deleteShoe = async (id) => {
        const del = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = { method: 'delete', };
        const response = await fetch(del, fetchConfig);
        if (response.ok) {
            newShoe();
        }
    }

    useEffect(() => {
        newShoe();
    }, []);

    if (shoes === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture}</td>
                            <td><div><button onClick={() => deleteShoe(shoe.id)}>Delete</button></div></td>
                            <td><div><img className="img-fluid img-thumbnail" src={shoe.picture} alt={shoe.model} model={{ width: '100px', height: '100px' }} /></div></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;
