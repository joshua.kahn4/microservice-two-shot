from django.shortcuts import render
from django.http import JsonResponse
from shoes_rest.models import Shoes
import json
from common.json import ModelEncoder
from shoes_rest.models import BinVO
from django.views.decorators.http import require_http_methods

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
                "closet_name",
                "bin_number",
                "bin_size",
                "import_href",
                  ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes,
    properties = [
        "name",
        "bin"
        ]
    encoders = {
        "bin": BinVOEncoder(),
    }

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture",
        "bin"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesDetailEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        bin = BinVO.objects.get(import_href=content["bin"])
        content["bin"] = bin
    except BinVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
        )
    shoes = Shoes.objects.create(**content)
    return JsonResponse(
        shoes,
        encoder=ShoesDetailEncoder,
        safe=False
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
        shoes,
        encoder=ShoesDetailEncoder,
        safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
    try:
        bin = BinVO.objects.get(import_href=content["bin"])
        content["bin"] = bin
    except BinVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
        )
    shoes = Shoes.objects.create(**content)
    return JsonResponse(
        shoes,
        encoder=ShoesDetailEncoder,
        safe=False,
    )
