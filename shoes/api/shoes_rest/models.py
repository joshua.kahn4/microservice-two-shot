from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    manufacturer = models.TextField(max_length = 100)
    model = models.CharField(max_length = 100)
    color = models.TextField(max_length = 100)
    picture = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )


    # def __str__(self):
    #     return self.shoe_id

    # def get_api_url(self):
    #     return reverse("api_show_shoes", kwargs={"id": self.id}) # type: ignore
